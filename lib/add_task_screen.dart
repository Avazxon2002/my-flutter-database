import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:my_tutorial/database_helper.dart';
import 'package:my_tutorial/notifications.dart';
import 'package:my_tutorial/task.dart';

class AddTaskScreen extends StatefulWidget {
  final Task? task;
  final Function? updateTaskList;

  const AddTaskScreen({this.updateTaskList, this.task});

  @override
  _AddTaskScreenState createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  final _formKey = GlobalKey<FormState>();
  String? _title = '';
  String? _hour, _minute, _time;
  DateTime _date = DateTime.now();
  late DateTime _dateWithTime;
  TimeOfDay _selectedTime = const TimeOfDay(hour: 00, minute: 00);
  String? _priority;
  //controller
  final TextEditingController _dateController = TextEditingController();
  final TextEditingController _timeController = TextEditingController();
  //mark
  final DateFormat _dateFormat = DateFormat('dd.MM.yyyy');
  final DateFormat _timeFormat = DateFormat('hh:mm');
  final List<String> _priorities = ['Low', 'Medium', 'High'];


  _handleDatePicker() async{
    final date = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: DateTime(2020),
        lastDate: DateTime(2050));

    if(date != _date){
      setState(() {
        _date = date as DateTime;
      });
      _dateController.text = _dateFormat.format(date!);
    }
  }

  _handleTimePicker() async{
    final timePicked = await showTimePicker(
        context: context,
        initialTime: _selectedTime,

    );

    if(timePicked != null){
      setState(() {
        _selectedTime = timePicked;
        _hour = _selectedTime.hour.toString();
        _minute = _selectedTime.minute.toString();
        _time = _hour! + " : " + _minute!;
        _dateWithTime = DateTime(
            _date.year, _date.month, _date.day,
            _selectedTime.hour, _selectedTime.minute
        );
        _timeController.text = _timeFormat.format(_dateWithTime);
      });
    }
  }

  // for save
  _submit(){
    if(_formKey.currentState!.validate()){
      _formKey.currentState!.save();
      Task task = Task(title: _title, date: _dateWithTime, priority: _priority);

      createReminderNotification(task);

      //Insert database logic
      if(widget.task == null){
        task.status = 0;
        DatabaseHelper.instance.insertTask(task);
      } else{

      // update database logic
        task.id = widget.task!.id;
        task.status = widget.task!.status;
        task.title = _title;
        task.date = _dateWithTime;
        task.priority = _priority;
        DatabaseHelper.instance.updateTask(task);
      }

      if(widget.updateTaskList != null) widget.updateTaskList!();
      Navigator.pop(context);
    }
  }

  //delete
  _delete(){
    DatabaseHelper.instance.deleteTask(widget.task!.id);
    widget.updateTaskList!();
    Navigator.pop(context);
  }

// For enter the update
@override
  void initState() {
    super.initState();
    if(widget.task != null){
      _title = widget.task!.title;
      _date = widget.task!.date;
      _priority = widget.task!.priority;
      _dateWithTime = widget.task!.date;
    }

    _dateController.text = _dateFormat.format(_date);
    _timeController.text = _timeFormat.format(_date);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      //AppBar
      appBar: AppBar(
        title: widget.task == null ?
          const Text("Add Task", style: TextStyle(color: Colors.white)):
          const Text("Update Task", style: TextStyle(color: Colors.white))
      ),

      //Body
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              Form(
                 key: _formKey,
                 child: Column(
                   children: [

                     //Title field
                     TextFormField(
                    decoration: const InputDecoration(
                       labelText: "Title",
                       hintText: "Write Title",
                    ),
                     onSaved: (input) => _title = input,

                     // validator
                     validator: (input) => input!.trim().isEmpty
                         ? 'Please enter task title'
                         : null,
                     initialValue: _title,
                   ),

                     //Date field
                     TextFormField(
                     controller: _dateController,
                     onTap: _handleDatePicker,
                     decoration: const InputDecoration(
                       labelText: "Date",
                       hintText: "Write Date",
                     ),

                     // validator
                     validator: (input) => _priority == null
                         ? 'Please select Date'
                         : null,
                   ),

                     //time field
                     TextFormField(
                       controller: _timeController,
                       onTap: _handleTimePicker,
                       decoration: const InputDecoration(
                         labelText: "Time",
                         hintText: "Write Time",
                       ),

                       // validator
                       validator: (input) => _priority == null
                           ? 'Please select Date'
                           : null,
                     ),

                      //Priority field
                      DropdownButtonFormField(
                     icon: const Icon(Icons.arrow_drop_down),
                     decoration: const InputDecoration(
                       labelText: "Priority",
                       hintText: "Write Priority"),
                     items: _priorities.map((priority) {
                       return DropdownMenuItem<String>(
                           value: priority,
                           child: Text(
                               priority,
                               style: const TextStyle(color: Colors.black)
                           )
                       );
                     }).toList(),
                     value: _priority, onChanged: (value) {
                       setState(() {
                         _priority = value! as String;
                       });
                     },

                     // validator(Tanlamasa hato beradi)
                     validator: (input) => _priority == null
                         ? 'Please select priority level'
                         : null,
                   ),
                    ]
                 ),
               ),

              // Save button
               TextButton(onPressed: _submit, child: const Text("Save")),

               //delete button
               TextButton(onPressed: _delete, child: const Text("Delete"))
           ],
          ),
        ),
      )
    );
  }
}