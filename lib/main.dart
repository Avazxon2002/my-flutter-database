import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_tutorial/task_list_screen.dart';

void main() {
  AwesomeNotifications().initialize('resource://drawable/img', [
    NotificationChannel(
    channelKey: 'scheduled_channel',
    channelName: 'Scheduled channel',
    channelDescription: 'Scheduled channel',
    channelShowBadge: true,
    defaultColor: Colors.teal,
    importance: NotificationImportance.High,
    )
  ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        unselectedWidgetColor: Colors.deepPurple[900]
      ),
      home: const TaskListScreen(),
    );
  }

}


