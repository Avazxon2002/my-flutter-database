import 'dart:io';
import 'package:my_tutorial/task.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper{
  static final DatabaseHelper instance = DatabaseHelper._instance();

  static Database? _db;

  DatabaseHelper._instance();

  String taskTable = 'task_table'; // it's Table name
  String colId = 'id';
  String colTitle = 'title';
  String colDate = 'date';
  String colPriority = 'priority';
  String colStatus = 'status';

  Future<Database?> get db async => _db ??= await _initDb();

  Future<Database> _initDb() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'todoList.db'; // todoList.db is File name
    final todoListDb = await openDatabase(path, version: 1, onCreate: _createDb);
    return todoListDb;
  }

  void _createDb(Database db, int version) async{
    await db.execute(
        'CREATE TABLE $taskTable('
            '$colId INTEGER PRIMARY KEY AUTOINCREMENT,'
            '$colTitle TEXT,'
            '$colDate TEXT,'
            '$colPriority TEXT,'
            '$colStatus INTEGER)'
    );
  }

  Future<List<Map<String, dynamic>>?> getTaskMapList() async {
      Database? db = await this.db;
      final List<Map<String, Object?>>? result = await db?.query(taskTable);
      return result;
    }

    //read function
  Future<List<Task>> getTaskList() async{
    final List<Map<String, dynamic>>? taskMapList = await getTaskMapList();
    final List<Task> taskList = [];
    taskMapList?.forEach((taskMap) {
      taskList.add(Task.fromMap(taskMap));
    });
    return taskList;

    }

    // add function
  Future<int?> insertTask(Task task) async{
    Database? db = await this.db;
    final int? result = await db?.insert(taskTable, task.toMap());
    return result;
    }

    // update function
  Future<int?> updateTask(Task task) async{
    Database? db = await this.db;
    final int? result = await db?.update(
        taskTable,
        task.toMap(),
        where: '$colId = ?',
        whereArgs: [task.id]);
    return result;
  }

  // delete function
  Future<int?> deleteTask(int? id) async{
    Database? db = await this.db;
    final int? result = await db?.delete(
        taskTable,
        where: '$colId = ?',
        whereArgs: [id]);
    return result;
  }

}