import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:my_tutorial/add_task_screen.dart';
import 'package:my_tutorial/database_helper.dart';
import 'package:my_tutorial/task.dart';

class TaskListScreen extends StatefulWidget {
  const TaskListScreen({Key? key}) : super(key: key);

  @override
  _TaskListScreenState createState() => _TaskListScreenState();
}

class _TaskListScreenState extends State<TaskListScreen> {
  final DateFormat _dateFormat = DateFormat('dd.MM.yyyy, hh:mm');
  late Future<List<Task>> _taskList;

  Widget _buildItem(Task task){
    return Container(
      color: Colors.cyan[50],
      child:
      ListTile(

        //List click
        onTap: () => Navigator.push(context, MaterialPageRoute(
            builder: (_) => AddTaskScreen(
              updateTaskList: _updateTaskList,
              task: task
            )
        )),

        // First text
        title: Text(task.title!, style: TextStyle(
              color: Colors.black87,
              decoration: task.status == 0
                  ? TextDecoration.none
                  : TextDecoration.lineThrough
          )),

        //Second text
        subtitle: Text(_dateFormat.format(task.date), style: TextStyle(
            color: Colors.black45,
            decoration: task.status == 0
                ? TextDecoration.none
                : TextDecoration.lineThrough
        )),

        //checkbox
        trailing: Theme(
          data: ThemeData(
            primarySwatch: Colors.blue,
            unselectedWidgetColor: Colors.black45
          ),
          child: Checkbox(
              value: task.status == 0 ? false : true,
            activeColor: Theme.of(context).primaryColor,
            onChanged: (bool? value) {
                if(value != null) task.status = value ?1: 0;
                DatabaseHelper.instance.updateTask(task);
                _updateTaskList();
            }
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      showDialog(
          context: context,
          builder: (context)=> AlertDialog(
            title: const Text('Allow Notification'),
            content: const Text('this app want to show notification'),
            actions: [

              // Don't Allow
              TextButton(
                onPressed: (){
                Navigator.pop(context);
                },
                child: const Text(
                    'Don`t Allow',
                    style: TextStyle(
                        color: Colors.green,
                        fontSize: 18)
                ),
              ),

              // Allow
              TextButton(
                onPressed: () =>
                AwesomeNotifications().requestPermissionToSendNotifications().then(
                        (_) => Navigator.pop(context)
                ),
                child: const Text(
                    'Allow',
                    style: TextStyle(
                        color: Colors.green,
                        fontSize: 18)
                ),
              ),
            ],
          )
      );
    });

    // notification created
    AwesomeNotifications().createdStream.listen((notification) {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Notification created'))
      );
    });

    // show badge
    AwesomeNotifications().actionStream.listen((event) {
      AwesomeNotifications().getGlobalBadgeCounter().then((value) =>
          AwesomeNotifications().setGlobalBadgeCounter(value));
    });

    _updateTaskList();
  }

  _updateTaskList(){
    setState(() {
      _taskList = DatabaseHelper.instance.getTaskList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      // Float action button
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => AddTaskScreen(updateTaskList: _updateTaskList)
                )
            );
          },
          child: const Icon(Icons.add)
      ),



      //body + AppBar
      body: FutureBuilder(
        future: _taskList,
        builder: (context, AsyncSnapshot snapshot){
          if(!snapshot.hasData){
            return const Center(child: CircularProgressIndicator(),);
          }

          return ListView.builder(
              itemCount: snapshot.data.length + 1,
              itemBuilder: (BuildContext context, int index) {

                //change selected lists count
                final int completedTaskCount = snapshot.data.where(
                        (Task task) => task.status == 1
                ).toList().length;

                if(index == 0){
                  return AppBar(
                    backgroundColor: Colors.blue,
                      title: Row(
                        children: [
                          const Text(
                            "My Tasks",
                            style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                          const SizedBox(width: 222,),
                          Text(
                            "$completedTaskCount/${snapshot.data.length}",
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                      ]
                      )
                  );
                } else {
                  return _buildItem(snapshot.data[index - 1]);
                }
              }
          );
        }
      ),
    );
  }
}
